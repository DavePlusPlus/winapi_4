#include <Windows.h>
#include "resource.h"

LRESULT CALLBACK fPrincipal(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR cmdline, int cShow) {
	HWND hPrincipal = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), NULL, fPrincipal);
	ShowWindow(hPrincipal, cShow);

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	while (GetMessage(&msg, NULL, NULL, NULL)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK fPrincipal(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	switch (msg) {
	case WM_COMMAND: {
		if ( LOWORD(wparam) == BTN_BUSCAR && HIWORD(wparam) == BN_CLICKED) {
			OPENFILENAME ofn;
			ZeroMemory(&ofn,sizeof(OPENFILENAME));
			char cDirFile[MAX_PATH] = "";

			ofn.hwndOwner = hwnd;
			ofn.lpstrFile = cDirFile;
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = "txt";
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR;
			ofn.lpstrFilter = "Archivos de Texto\0*.txt\0Imagenes BMP\0*.bmp\0Todos los Archivos\0*.*\0";
			if (GetOpenFileName(&ofn)) {
				HWND hPControl = GetDlgItem(hwnd, PC_IMAGE);
				HBITMAP hImage = (HBITMAP) LoadImage(NULL, cDirFile, IMAGE_BITMAP, 100, 100, LR_LOADFROMFILE);
				SendMessage(hPControl, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM) hImage);
			}
		}
	}break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(117);
		break;
	}
	return FALSE;
}

